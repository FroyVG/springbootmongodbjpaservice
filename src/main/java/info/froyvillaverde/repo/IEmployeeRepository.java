package info.froyvillaverde.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import info.froyvillaverde.entities.Employee;

@Repository
public interface IEmployeeRepository extends MongoRepository<Employee, String>{

}
