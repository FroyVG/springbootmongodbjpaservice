package info.froyvillaverde.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import info.froyvillaverde.entities.Employee;
import info.froyvillaverde.repo.IEmployeeRepository;

@Service
public class EmployeeServiceImpl implements IEmployeeService{
	
	@Autowired
	IEmployeeRepository employeeRepository;

	@Override
	public List<Employee> getAllEmployees() {
		
		List<Employee> employees = null;
		employees = employeeRepository.findAll();
		return employees;
	}

	@Override
	public void saveEmployees(Employee employee) {

		employeeRepository.save(employee);
	}

	@Override
	public void updateEmployee(Employee employee) {

		employeeRepository.save(employee);
	}

	@Override
	public void deleteEmployees(String employeeId) {

		employeeRepository.deleteById(employeeId);
	}

}
