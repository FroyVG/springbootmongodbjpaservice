package info.froyvillaverde.service;

import java.util.List;

import info.froyvillaverde.entities.Employee;

public interface IEmployeeService {

	public List<Employee> getAllEmployees();

	public void saveEmployees(Employee employee);

	public void updateEmployee(Employee employee);

	public void deleteEmployees(String employeeId);

}
