package info.froyvillaverde.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import info.froyvillaverde.entities.Employee;
import info.froyvillaverde.service.EmployeeServiceImpl;

@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeServiceImpl employeeService;
	
	@RequestMapping(value = "/saveEmployee", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> saveEmployee(@RequestBody Employee employee){
		
		employeeService.saveEmployees(employee);
		
		
		return new ResponseEntity<Object>("Successfully saved", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateEmployee", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> updateAllEmployees(@RequestBody Employee employee) {
		
		employeeService.updateEmployee(employee);
		return new ResponseEntity<Object>("Update all Employees successfully",HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/deleteEmployees/{employeeId}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<Object> deleteAllEmployees(@PathVariable String employeeId) {

		employeeService.deleteEmployees(employeeId);
		return new ResponseEntity<Object>("Deleted Employee Successfully", HttpStatus.OK);
	}

}
